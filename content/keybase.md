+++
title = "keybase"
date = "2018-03-20"
outputs = "PlainTextFile"
+++
==================================================================
https://keybase.io/osowoso
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://zenobit.gitlab.io
  * I am osowoso (https://keybase.io/osowoso) on keybase.
  * I have a public key ASCusFwHdta_1KuISfNmzWMyWk1ONskDnzCImxHBEbrGcgo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0120e2a5a407dfa87b1d5ce684f50b0c33138a3258efbe806c381cb9dda47c9d4e4a0a",
      "host": "keybase.io",
      "kid": "0120aeb05c0776d6bfd4ab8849f366cd63325a4d4e36c9039f30889b11c111bac6720a",
      "uid": "aa894aaf40e88857a17029c548b20519",
      "username": "osowoso"
    },
    "merkle_root": {
      "ctime": 1633910708,
      "hash": "252ebb63577b531dd05d635bccf246f285a7e2c8885fdb266c2fc314bde670d785e8e3570442081d4cc5b5fa1b601176860e031439bb2053708471407001ebee",
      "hash_meta": "532b0ef06b2d2710a95d06b669f6dce2102ae488c552bd15e1ec92a6ce562aa2",
      "seqno": 20956406
    },
    "service": {
      "entropy": "IIuGKoBkjU0dst5pNSRsyBTG",
      "hostname": "zenobit.gitlab.io",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.8.1"
  },
  "ctime": 1633910740,
  "expire_in": 504576000,
  "prev": "92522f490a24d23907dad785e3a57e88fc4dd2e817beb0e04a792338f4dbd01a",
  "seqno": 15,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgrrBcB3bWv9SriEnzZs1jMlpNTjbJA58wiJsRwRG6xnIKp3BheWxvYWTESpcCD8QgklIvSQok0jkH2teF46V+iPxN0ugXvrDgSnkjOPTb0BrEIMbwzp8BhwOrZh0fy0TAEcMD/UXGUAh+l1MIhtFRk/89AgHCo3NpZ8RA9mkOd7fiiP6py27bUdSNJNUTs4ctcck2oOkEoXZ1NvQIJaChmXD2khN79a/KT938j0rqSoyfVTNlw42dACx5BqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIG2iA+CjKzEKne+rNa7EE7x6F4qn2WlwKo/K3cLVcqjUo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/osowoso

==================================================================
