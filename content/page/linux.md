---
title: linux
subtitle: Everything about linux operating systems
comments: true
---

I think best linux distribution out there is...
- Void linux - https://voidlinux.org

- Void is a general purpose operating system, based on the monolithic Linux kernel. Its package system allows you to quickly install, update and remove software; software is provided in binary packages or can be built directly from sources with the help of the XBPS source packages collection.

It is available for a variety of platforms. Software packages can be built natively or cross compiled through the XBPS source packages collection. 


my Void linux related gitlab group:
- https://gitlab.com/awesome-void
