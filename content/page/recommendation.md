---
title: recommendation
subtitle: Some useful software, links...
comments: true
---

# Browser

![firefox](/recommendation/firefox.png)
 Mozilla Firefox or simply Firefox, is a free and open-source web browser developed by the Mozilla Foundation and its subsidiary, the Mozilla Corporation. Firefox uses the Gecko rendering engine to display web pages, which implements current and anticipated web standards.
- firefox - https://www.mozilla.org/en-US/

![Brave browser](/brave.png)
 The browser reimagined.
 Three times faster than Chrome. Better privacy by default than Firefox. Uses 35% less battery on mobile.

- chrome based Brave browser - https://brave.com/
-----------------------

# Reddit alternative frontend

![libreddit](/libreddit.png)
 Libreddit is a portmanteau of "libre" (meaning freedom) and "Reddit". It is a private front-end like Invidious but for Reddit. Browse the coldest takes of r/unpopularopinion without being tracked.
- rocket Fast: written in Rust for blazing fast speeds and memory safety
- cloud Light: no JavaScript, no ads, no tracking, no bloat
- detective Private: all requests are proxied through the server, including media
- lock Secure: strong Content Security Policy prevents browser requests to Reddit

- https://libredd.it - libreddit - https://github.com/spikecodes/libreddit
# together with browser addon for firefox, chrome ...
![PrivacyRedirect](/privacyredirect.png)
 A simple web extension that redirects Twitter, YouTube, Instagram & Google Maps requests to privacy friendly alternatives.
- PrivacyRedirect - https://github.com/SimonBrazell/privacy-redirect
-----------------------

# Reddit alternative
![lemmy.ml](/lemmy.png)
- Lemmy is similar to sites like Reddit, Lobste.rs, or Hacker News: you subscribe to communities you're interested in, post links and discussions, then vote and comment on them. Lemmy isn't just a reddit alternative; its a network of interconnected communities ran by different people and organizations, all combining to create a single, personalized front page of your favorite news, articles, and memes.
- https://join-lemmy.org/
-----------------------

# Twitter alternative
![mastodon](/mastodon.png)
- Social networking, back in your hands
- Follow friends and discover new ones among more than 4.4M people. Publish anything you want: links, pictures, text, video. All on a platform that is community-owned and ad-free.
- mastodont - https://mastodon.social
- my choosen host: https://distrotoot.com
    Thanks to DT...
-----------------------

# WhatsApp alternative
![element](/element.png)
- Own your conversations.
 Secure and independent communication, connected via Matrix

- Matrix is an open source project that publishes the Matrix open standard for secure, decentralised, real-time communication,  -and its Apache licensed reference implementations.
 Maintained by the non-profit Matrix.org Foundation, we aim to create an open platform which is as independent, vibrant and evolving as the Web itself... but for communication.
![matrix](/matrix.png)
- https://element.io/ - Element on matrix network - https://matrix.org/
-----------------------

# YouTube alternative
![odysee](/odysee.png)
- LBRY is a blockchain-based file-sharing and payment network that powers decentralized platforms, primarily social networks and video platforms. LBRY's creators also run Odysee, a video-sharing website that uses the network. Video platforms built on LBRY, such as Odysee, have been described as decentralized alternatives to YouTube. The company has described Odysee and other platforms it has built utilizing its LBRY protocol as platforms for free speech and lightly moderates content, including removing pornography or the promotion of violence and terrorism.
![lbry](/lbry.png)
- odysee - https://odysee.com/$/invite/@zenobit:6 on Lbry - https://lbry.com/
-----------------------

# Password manager
![Bitwarden](/bitwarden.png)
 Platform Features and Benefits:
- Secure Password Sharing
- Cross-Platform Accessibility
- Cloud-Based or Self-Host
- Security Audit & Compliance
- Vault Health Reports
- Directory Sync
- Always-On Support
- Detailed Event Logs
- Flexible Integrations

- Bitwarden - https://bitwarden.com/
-----------------------

# Share your dotfiles
![dotshare.it](/dotshare.png)
- DotShare is the attempted solution to all of the sporadic "Post your X configs!" threads on various distro's forums. Moving all of those threads to one central location, people can follow and get updates from users who often post configs they like or share their tastes in setups. In the end making it much easier to find what interests them and the new cool things people are coming up with.

- http://dotshare.it/
-----------------------

# Test a new operating system
![distrotest.net](/distrotest.png)
- Welcome to DistroTest.net
 On our website you will find many operating systems,
 which you can test directly online without a installation.

 There are no restrictions for the operating system:
 You can use all functions of the system,
 Uninstall and install software, test installed programs and
 even delete or format the hard disk or system files...

- Distrotest - https://distrotest.net
-----------------------
